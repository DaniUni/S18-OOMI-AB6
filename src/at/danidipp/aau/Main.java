package at.danidipp.aau;

public class Main {

    public static void main(String[] args) {
        test(new Spezialsauce(new Gurke(new Burger())));
        test(new Senf(new Krautsalat(new Spezialsauce(new Bratwurst()))));
        test(new Bratwurst());
        test(new Spezialsauce(new Hendl()));
    }

    private static void test(Gericht gericht){
        gericht.druckeBeschreibung();
        System.out.println("\nKosten: "+gericht.berechneKosten());
        System.out.println();
    }
}
