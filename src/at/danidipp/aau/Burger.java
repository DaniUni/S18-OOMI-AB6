package at.danidipp.aau;

public class Burger extends Gericht {

	public double berechneKosten() {
		return 5.50d;
	}

	public Burger() {
        super();
        super.beschreibung = "Burger";
	}

}